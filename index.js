/*demo*/
document.getElementById("btn1").addEventListener('click', () => {
 alert("add more!");
});

let parag1 = document.getElementById("para1"); 
let parag2 = document.getElementById("para2"); 

document.getElementById("btn2").addEventListener('click', () => { parag1.innerHTML = "I can even do this";});
document.getElementById("btn3").addEventListener('click', () => {
parag2.innerHTML = "sumuku kana";
parag2.style.color = "purple";
parag2.style.fontSize = "50px";
});

/*Lesson Proper
	Topics
		-Introduction
		-Writing Comments
		- Syntax and Statements
		-Peeking at Variable's value through console
		-Data types
		-Functions
	JavaScripts
		- is a scripting language the enables you to make interactive web pages
		- was originally intendede for web browsers however, the are now als integrated in web
		servers through the use of Node.js.
		- it is one core technologies of the world wide web

	Uses of java script
		-web app 
		-flashgames
		-art creation
		-mobile app
		*/
// writing comments in js
// there are two ways of writing comments in Js
	// single line
	//sample comments - can only make comments in single line

	/* 
		multi-line comments:
		  ctrl + shift + /

	*/
/*
	statements
	-Statements are instruction, expressions we add to our programming language
	which will then be communicated to our computes
	- statements in js commonly ends in semi-colon(;). however,js has an implemented way of automatically
	adding semicolons at the end of statements. Which, therefore, mean that, unlike other languages
	- ; end marks 
	
	syntax
	-syntax in programming, is a set of rulles that describes how statements are properly made constructed
	- lines/block of code must follow a certain rules for it ot work. because remember, you are communicting with a computer
*/


// Mini-Activity
let firstName = "Julien";
let lastName = "Rosales";

let fullName = firstName + " " + lastName;
console.log(fullName);

let word1 = "is";
let word2 = "student";
let word3 = "of";
let word4 = "DLSUD";
let space = " ";

let sentence = fullName + space + word1 + space + word2 + space + word3 + space
 + word4;

console.log(sentence);

let numString1 =  "5";
let numString2 =  "6";
let num1 =  5;
let num2 =  6;

console.log(numString1 + numString2);
console.log(num1 + num2);

let num3 = 5.5;
let num4 = .5;

console.log(num1 + num3);
console.log(num3 + num4);

